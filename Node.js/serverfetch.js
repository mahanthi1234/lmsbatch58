let express = require('express')
let app=express()
let bodyparser=require('body-parser');
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended:false}))
app.use('/fetch',(require('./components/fetch')));
app.use('/register',require('./components/register'));
app.listen(3201,(req,res)=>{
console.log("port listening at 3200")
})

