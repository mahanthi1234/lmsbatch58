//continue and break
//break is used to stop the execution or loop
//continue skip the statement execution

for(let i = 0 ; i <=10; i++){
    if (i==5){
        break
    }
    console.log(i)
}


for(let i = 0 ; i <=10; i++){
    if(i==5){
        continue
    }
    console.log(i)
}